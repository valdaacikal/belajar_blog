<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => 'Jelaskan perbedaan linear & relative layout!',
            'content' => '-	Linear Layout adalah jenis layout dimana user menempatkan 1 objek (widget) per baris/kolom. Jadi di dalam setiap baris/kolom hanya ada 1 objek (widget) yang kita tempatkan . Nahh di Linear Layout ini ada dua jenis . Yaitu :
            •	Vertical Linear Layout : Apabila user menempatkan 1 widget (objek) per baris
            •	Horizontal Linear Layout : Apabila user menempatkan 1 objek per kolom
            -	Relative Layout adalah layout yang penataan nya ini adalah penataan yang menempatkan widget-widget didalamnya seperti layer, sehingga sebuah widget dapat berada di atas/di bawah widget lainnya atau dengan kata lain Relative merupakan layout yang penataannya lebih bebas (Relative) sehingga bisa di tata di mana saja. ',
            'category' => 'Layout'
            ]);
    }
}
